FROM ubuntu:20.04

ENV DOCKER_VERSION 5:20.10.6~3-0~ubuntu-focal
ENV DOCKER_COMPOSE_VERSION 1.29.1
ENV LANDO_VERSION v3.6.4

RUN apt-get update && apt-get install -y \
    mysql-client \
	apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    unzip \
    sudo \
    software-properties-common \
 && rm -rf /var/lib/apt/lists/*

# Install Docker
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
 && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
 && apt-get update \
 # Set the docker group to a consistent value.
 && groupadd -g 5000 docker \
 && apt-get install -y docker-ce=$DOCKER_VERSION \
 && rm -rf /var/lib/apt/lists/* \
 && docker --version \
 && curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose \
 && chmod a+x /usr/local/bin/docker-compose \
 && docker-compose --version

# Install Lando
RUN curl -L -o /tmp/lando.deb https://github.com/lando/lando/releases/download/$LANDO_VERSION/lando-x64-$LANDO_VERSION.deb \
 && dpkg -i /tmp/lando.deb \
 && rm -f /tmp/lando.deb

# Copy script for making fake tty.
COPY faketty /usr/local/bin/

# Add a Lando user
RUN useradd -ms /bin/bash lando \
 && usermod -a -G docker lando \
 && echo 'lando ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/lando \
 && mkdir /home/lando/.lando \
 && mkdir /home/lando/.lando/plugins \
 && chown -R lando:lando /home/lando

# Copy our MTU plugin.
COPY mtu /home/lando/.lando/plugins/mtu

USER lando
WORKDIR /home/lando

