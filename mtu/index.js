'use strict';

module.exports = (lando) => {

  lando.events.on('post-bootstrap-engine', () => {
    // This will get the 'lando_bridge_network'.
    lando.engine.createNetwork = function (name) {
      return this.docker.createNet(name, {
        Options: {
          'com.docker.network.driver.mtu': '1400'
        }
      });
    };
  });

  return {};
};

