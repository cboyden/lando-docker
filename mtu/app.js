'use strict';

module.exports = (app, lando) => {
  app.add(new app.ComposeService('mtu', {}, {
    networks: {
      default: {
        driver_opts: {
          'com.docker.network.driver.mtu': 1400,
        }
      }
    }
  }));

  return {}
};
